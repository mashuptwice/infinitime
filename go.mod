module go.arsenm.dev/infinitime

go 1.16

require (
	github.com/fxamacker/cbor/v2 v2.4.0
	github.com/godbus/dbus/v5 v5.0.6
	github.com/muka/go-bluetooth v0.0.0-20220819140550-1d8857e3b268
	github.com/rs/zerolog v1.26.1
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
)
