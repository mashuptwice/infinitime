package player

import (
	"fmt"
	"os/exec"
)

// VolUp uses pactl to increase the volume of the default sink
func VolUp(percent uint) error {
	return exec.Command("pactl", "set-sink-volume", "@DEFAULT_SINK@", fmt.Sprintf("+%d%%", percent)).Run()
}

// VolDown uses pactl to decrease the volume of the default sink
func VolDown(percent uint) error {
	return exec.Command("pactl", "set-sink-volume", "@DEFAULT_SINK@", fmt.Sprintf("-%d%%", percent)).Run()
}
